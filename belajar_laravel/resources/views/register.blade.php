<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
   

    <title>Register</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
  </head>

  <body class="limiter">
  <form class="form-signin" method="POST" action="{{ route('register') }}">
    {{ csrf_field() }}
  <div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-02.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					 Register {{ config('app.name') }}
				</span>
     
      <label for="inputEmail" class="sr-only">Name</label>
      <input type="text" name="name" id="inputName" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="Please Insert Your Name" value="{{ old('name') }}" required autofocus>

      @if ($errors->has('name'))
  <div class="invalid-feedback">
  {{ $errors->first('name')}}
 
 </div>      
  @endif

      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" name="email" id="inputEmail" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Email address" value="{{ old('email') }}" required autofocus>
 
      @if ($errors->has('email'))
      <div class="invalid-feedback">
      {{ $errors->first('email')}}
     
     </div>      
      @endif
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" name= "password" id="inputPassword" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password" required>
      @if ($errors->has('password'))
      <div class="invalid-feedback">
      {{ $errors->first('password')}}
     
     </div>      
      @endif
      <label for="inputPassword" class="sr-only">Password Confirmation</label>
      <input type="password" name= "password_confirmation" id="inputPassword" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"  placeholder="Password confirmation" required>
      @if ($errors->has('password_confirmation'))
      <div class="invalid-feedback">
      {{ $errors->first('password_confirmation')}}
     
     </div>      
      @endif
      <div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">
							Register
						</button>
					</div><br>
      <p class="text-center">&copy; 2020-2021</p>
    </form>
</div>
</div>
</div>
  
      </body>
</html>